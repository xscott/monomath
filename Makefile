CXXFLAGS = -std=c++11 -fwrapv -O3 -Wall -Wextra -fmax-errors=2
LDFLAGS = 

PROGRAMS = \
    example0 \
    example1 \
    example2 \
    example3 \

.PHONY: all clean

all: $(PROGRAMS)

%: %.cc monomath.h
	@echo Compiling $@
	@$(CXX) $(CXXFLAGS) $< -o $@ $(LDFLAGS)

clean:
	rm -f $(PROGRAMS) *.ppm *.pgm

