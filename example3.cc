#include <stdio.h>
#include "monomath.h"

struct Alpha { uint8_t val; };

Alpha blend(Alpha, Alpha, long amount) {
    if (amount > 255) amount = 255;
    return (Alpha){ (uint8_t)amount };
}

int main() {
    const long width = 800, height = 200;
    static Alpha pixmap[width*height];
    Alpha ignored;
    monomath::Font font;
    monomath::Options opts;
    opts.width = 40;
    opts.height = 200;
    opts.weight = 2.0;
    opts.spacing = 0.6;

    font.draw(
        pixmap, width, height, ignored,
        20, 20, "Composite It", opts
    );

    FILE* pgm = fopen("example3.pgm", "w");
    fprintf(pgm, "P5 %ld %ld 255\n", width, height);
    fwrite(pixmap, 1, width*height, pgm);
    fclose(pgm);

    return 0;
}

