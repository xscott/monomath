#include <stdio.h>
#include "monomath.h"

int main() {
    const long width = 800, height = 600;
    static monomath::RGB pixmap[width*height];
    monomath::RGB color = { 0xFF, 0xCC, 0x00 };
    monomath::Font font;
    monomath::Options opts;

    opts.degrees = 20;
    opts.oblique = 1.0;
    opts.width = 50;
    opts.height = 100;
    opts.weight = 0.5;
    font.draw(
        pixmap, width, height, color,
        20, 240, " Oblique\nNarrow,and\n Rotated", opts
    );

    FILE* pgm = fopen("example2.ppm", "w");
    fprintf(pgm, "P6 %ld %ld 255\n", width, height);
    fwrite(pixmap, 1, width*height*3, pgm);
    fclose(pgm);

    return 0;
}
