#include <stdio.h>
#include "monomath.h"

int main() {
    const long width = 800, height = 100;
    static monomath::RGB pixmap[width*height];
    monomath::RGB color = { 0xFF, 0xFF, 0xFF };
    monomath::Font font;
    monomath::Options opts;

    font.draw(
        pixmap, width, height, color, 25, 40,
        "A simple, scalable, vector font", opts
    );

    FILE* pgm = fopen("example0.ppm", "w");
    fprintf(pgm, "P6 %ld %ld 255\n", width, height);
    fwrite(pixmap, 1, width*height*3, pgm);
    fclose(pgm);

    return 0;
}

