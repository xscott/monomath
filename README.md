# monomath.h

## Single file header library for rendering monospace text and math

This library was originally part of larger project I was working on for
numerical methods and data visualization using the Rust programming language.
I wanted a simple way to render text for the axes on plots and other graphs,
and I didn't want to incorporate a large library like FreeType or similar.
My Rust project is on back burner for the indefinite future, but I've extracted
the font/glyph rendering code and converted it to C++ at the request of a
friend.

Here is an example of how to use it:

```c++
    #include <stdio.h>
    #include "monomath.h"

    int main() {
        const long width = 800, height = 100;
        static monomath::RGB pixmap[width*height];
        monomath::RGB color = { 0xFF, 0xFF, 0xFF };
        monomath::Font font;
        monomath::Options opts;

        font.draw(
            pixmap, width, height, color, 25, 40,
            "A simple, scalable, vector font", opts
        );

        FILE* pgm = fopen("example0.pgm", "w");
        fprintf(pgm, "P6 %ld %ld 255\n", width, height);
        fwrite(pixmap, 1, width*height*3, pgm);
        fclose(pgm);

        return 0;
    }
```

![example0.png](img/example0.png "example0.cc")

## Design

The glyphs are all stored in a compact (and slightly cryptic) array of strings
in the header file.  Given my original limited intent to use this for
mathy/sciency things, I created glyphs for the Ascii character set, the Greek
alphabet, and some odds and ends along with a bunch of mathematical symbols I
thought might be useful.  There are only 312 glyphs, some of which are used for
multiple code points, so **this is not a full featured font for rendering
arbitrary Unicode text**.  Any Unicode code point for which I don't have a
glyph will be rendered as an empty rectangle, as shown in the top left of
this image.

![example1.png](img/example1.png "example1.cc")

The `Font::draw` function is templatized on the type of text strings it
accepts.  Internally it always uses 32 bit codes, but it accepts strings of
ASCII, UCS-2, and UCS-4 data.  It does **not** work with UTF-8 data outside
of the ASCII subset, and it does not work with UTF-16 surrogates.

Most modern font libraries use cubic Bezier curves as outlines for the glyphs.
This library uses quadratic Beziers as the lines of the glyph itself.  In order
to render efficiently, the glyphs are compiled to a *signed distance field*.
Building the signed distance fields is not super fast, and each glyph uses some
memory, so the Font object creates and caches them as needed.  In other words,
you should probably instantiate one Font object for your program and re-use the
one instance as needed.  The Font object is not thread safe, so if you draw
text from multiple threads, you should create a separate Font for each thread.

The `Options` struct allows you to change the width and height of the glyphs
along with other parameters.  The typography world has a long history and
some exotic terminology and units of measurement.  I've borrowed some of
their words, but I don't claim to use them according to the conventions
you may have seen elsewhere.  Here are the fields of the `Options` struct,
along with their default values:

```c++
    struct Rect { long l, t, r, b; };

    struct Options {
        double width = 16;     // glyph width in pixels
        double height = 32;    // glyph height in pixels
        double weight = 1.0;   // light: 0.5, very heavy 4.0
        double spacing = 0.5;  // horizontal spacing of chars
        double leading = 0.25; // vertical spacing of lines
        double oblique = 0.0;  // 0.5 for faux italics
        double degrees = 0.0;  // counter clockwise rotation
        // Restrict drawing to { left, top, right, and bottom }
        Rect clip = { LONG_MIN, LONG_MIN, LONG_MAX, LONG_MAX };
    };
```

Here is an example showing some of the features:

```c++
    opts.degrees = 20;
    opts.oblique = 1.0;
    opts.width = 50;
    opts.height = 100;
    opts.weight = 0.5;
    font.draw(
        pixmap, width, height, color,
        20, 240, " Oblique\nNarrow,and\n Rotated", opts
    );
```

![example2.png](img/example2.png "example2.cc")

The `Font::draw` function is also templatized on the source and destination
pixel types.  There are many ways to do blending and many different pixel
formats in the wild.  This library provides a `monomath::RGB` type with 3 bytes
per pixel because that makes it easy to debug with the PPM file format.
However you can support other pixel formats by implementing your own `blend`
function, and `Font::draw` will C++'s *argument dependent lookup* to call it as
needed.

The `blend` function will receive three arguments: 0) the destination color in
the original image, 1) the color you've passed into `Font::draw`, and 2) an
integer `amount` in the range of 0 to 256 inclusive.  The `amount` will be 0 to
indicate completely transparent, and 256 to indicate completely opaque.

Here is an example which ignores the source and destination color and simply
extracts the `amount` parameter as an alpha channel for compositing:

```c++
    struct Alpha { uint8_t val; };

    Alpha blend(Alpha, Alpha, long amount) {
        if (amount > 255) amount = 255;
        return (Alpha){ (uint8_t)amount };
    }
```

![example3.png](img/example3.png "example3.cc")

Creating your own pixel types and corresponding blend functions allows you to
do transparency and other effects, but this is left as an exercise for the
reader.

## License

If you find this library useful, that's great.  I've licensed it under the
Mozilla Public License version 2.0 **with the "Incompatible With Secondary
Licenses" notice**.  I want people to be able to use this freely for almost any
purpose, including commercial usage, but I do not want people to make additions
or changes which use a different license.  In other words, I chose the MPL with
the no secondary licenses clause because I think it's a good way to enforce a
"Share Alike" restriction.

## Contributing

I am not interested in contributions, and I will not accept pull requests.  I
*will* consider a politely worded bug report, but if I think it's rude or
demanding I will simply close it as not worth fixing.  Please remember: I
released this library as a gift, and I don't owe you anything.  This project is
complete from my point of view, so it is not actively maintained, and I have no
plans to add features in the future.


