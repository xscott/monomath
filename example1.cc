#include <stdio.h>
#include <vector>
#include "monomath.h"

int main() {
    const long width = 800, height = 600;
    static monomath::RGB pixmap[width*height];
    monomath::RGB color = { 0xFF, 0xFF, 0xFF };
    monomath::Font font;
    monomath::Options opts;

    std::vector<char32_t> text;
    text.push_back(1); // missing glyph
    for (long ii = 1; monomath::GLYPH_TABLE[ii].codes != 0; ++ii) {
        if (ii%26 == 0) text.push_back('\n');
        text.push_back(monomath::GLYPH_TABLE[ii].codes[0]);
    }
    text.push_back(0);

    font.draw(
        pixmap, width, height, color, 80, 60,
        text.data(), opts
    );

    FILE* pgm = fopen("example1.ppm", "w");
    fprintf(pgm, "P6 %ld %ld 255\n", width, height);
    fwrite(pixmap, 1, width*height*3, pgm);
    fclose(pgm);

    return 0;
}


